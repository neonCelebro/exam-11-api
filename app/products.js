const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Product = require('../models/Product');
const Category = require('../models/Category');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
  // Product index
  router.get('/', async (req, res) => {
    const products = await Product.find();
    const category =  await Category.find();
    res.send({products, category});
  });

  router.post('/', upload.single('image'), (req, res) => {
    const productData = req.body;

    if (req.file) {
      productData.image = req.file.filename;
    } else {
      productData.image = null;
    }

    const product = new Product(productData);

    product.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  // Product get by ID
  router.get('/:id', async (req, res) => {
    const products = await Product.find({category: req.params.id});
    const category = await Category.find();
    res.send({products, category});
  });

    router.get('/product/:id', async (req, res) => {
        Product.findOne({_id: req.params.id}).populate('userID category')
            .then(product => res.send(product))
            .catch(error => res.status(400).send(error))
    });

    router.delete('/product/:id', (req, res) => {
        Product.remove({_id: req.params.id})
            .then(product => res.send(product))
            .catch(error => res.status(400).send(error))
    });

  return router;
};

module.exports = createRouter;