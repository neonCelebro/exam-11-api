const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Product = require('./models/Product');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('categories');
        await db.dropCollection('users');
        await db.dropCollection('products');

    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [Food, Clothes, Gadgets, Cars] = await Category.create({
            title: 'Food',
            description: 'All that refers to food'
        }, {
            title: 'Clothes',
            description: 'Everything to clothes'
        }, {
            title: 'Gadgets',
            description: 'Gadgets and other electronic'
        }, {
            title: 'Cars',
            description: 'Muscule and evil cars'
        }
    );

    const [user1, user2] = await User.create({
        username: 'user-1',
        displayName: 'User_1',
        password: '1234',
        phoneNumber: '+996555555555'
    }, {
        username: 'user-2',
        displayName: 'User_2',
        password: '1234',
        phoneNumber: '+996777123123'
    });

    await Product.create({
        title: 'Aples',
        description: 'Very cool apls',
        category: Food._id,
        image: 'aple.jpg',
        userID: user1._id,
        price: 200
    }, {
        title: 'Toyota Harrier',
        description: '2003, v3, 4wd',
        category: Cars._id,
        image: 'car.jpg',
        userID: user2._id,
        price: 3000000
    });

    db.close();
});